require(["jquery",'jquery/ui','mage/mage'],function($) {
    $(document).ready(function() 
    {

       var importURL = '';
       var cartURL= '';
       var cartToken = '';
       var checkProgressURL = $(document).find("input[name='progressURL']").val();
       var checkProgressVar;


        $(document).on('change','input#select-all',function()
        {
            if(this.checked) 
            {
                $('input.checkbox').attr('checked', true);
            }
            else
            {
                $('input.checkbox').attr('checked', false);
            }
        });

      
       /*
        *  SUBMIT SOURCE CART SETUP FORM
        *  @return html for configuration tab
        */

        $("#form-setup-submit").click(function(event){
            event.preventDefault(); 
            var setupForm = $('#form-setup');
            if (setupForm.validation('isValid')== true) 
            {
                var setupurl = $('#form-setup').attr('action');
                $.ajax({
                    url: setupurl,
                    type: 'POST',
                    dataType: 'json',
                    showLoader: true,
                    data: $('#form-setup').serialize(),
                    complete: function(response) { debugger            
                        var response = response.responseJSON.output;
                        if(response.result!='error')
                        {
                            cartURL = $('input[name="cart_url"]').val();
                            cartToken = $('input[name="cart_token"]').val(); 
                            $('#step-config').html(response);
                            // activate configuration tab
                            $('#migration_container').tabs( "enable", 1 );
                            $('#migration_container').tabs("option", "active", 1);
                        } 
                        else
                        {
                            alert(response.message);
                        }                       
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log('Error happens. Try again.');
                    }
                });
            }
        });


       /*
        *   SUBMIT CONFIGURATION FORM
        *   @return html for migration tab and start importing 'Store Info'
        */

        $(document).on('click','#form-config-submit',function(event){
            event.preventDefault(); 

            if(!$('input.checkbox').not('input[name="select-all"]').is(':checked'))
            {
                alert("select atleast one entity to migrate");
                return false;
            }

            var configurl = $('#form-config').attr('action');
            importURL = $('input[name="importURL"]').val();
            //var cart_url = $('input[name="cart_url"]').val();
            //var cart_token = $('input[name="cart_token"]').val();
            $.ajax({
                url: configurl,
                type: 'POST',
                dataType: 'json',
                showLoader: true,
                data: $('#form-config').serialize(),
                complete: function(response) 
                { debugger            
                    var html = response.responseJSON.output;
                    $('#step-migrate').html(html);
                    // activate migration tab
                    $('#migration_container').tabs( "enable", 2 );
                    $('#migration_container').tabs("option", "active", 2);

                    setTimeout(clearStore, 2000);

                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        });

        /**
        *  CLEAR STORE DATA
        *  @return 
        */
        function clearStore()
        {
            $.ajax({
                url: importURL,
                type: 'POST',
                dataType: 'json',
                //showLoader: true,
                data: "cart_url="+cartURL+"&cart_token="+cartToken+"&action=clear_store",
                complete: function(response) 
                { debugger            
                    var response = response.responseJSON.output;
                    if(response.status == 'success')
                    {
                        setTimeout(importStoreInfo, 2000);    
                    }         
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        }


       /**
        *  IMPORT STORE GENERAL DATA/ STORE EMAILS / SALES EMAIL
        *  @return status of import 'store info' and start importing "TAX" and checking progress
        */
        function importStoreInfo()
        {
            $.ajax({
                url: importURL,
                type: 'POST',
                dataType: 'json',
                //showLoader: true,
                data: "cart_url="+cartURL+"&cart_token="+cartToken+"&action=import_store",
                complete: function(response) 
                { debugger            
                    var response = response.responseJSON.output;
                    if(response.status == 'success')
                    {
                        setTimeout(checkProgress,2000);
                        setTimeout(importTaxes, 500);      
                    }         
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        }


       /*
        *  IMPORT TAXES
        */
        function importTaxes()
        {
            $.ajax({
                url: importURL,
                type: 'POST',
                dataType: 'json',
               // showLoader: true,
                data: "cart_url="+cartURL+"&cart_token="+cartToken+"&action=import_tax",
                complete: function(response) 
                { debugger            
                    var response = response.responseJSON.output;
                    if(response.status == 'success')
                    {
                       setTimeout(importCategories, 3000);
                    }
                    else
                    {
                       //clearInterval(importTaxes(importURL    ,cart_url,cart_token), 2000);
                    }         
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        }


       /*
        *  IMPORT CATEGORIES
        */
        function importCategories()
        {    
            $.ajax({
                url: importURL,
                type: 'POST',
                dataType: 'json',
                data: "cart_url="+cartURL+"&cart_token="+cartToken+"&action=import_categories",
                complete: function(response) 
                { debugger            
                    var response = response.responseJSON.output;
                    if(response.status == 'success')
                    {
                       //setTimeout(importCategories, 500);
                    }
                    else
                    {
                       //clearInterval(importTaxes(importURL,cart_url,cart_token), 2000);
                    }         
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        }


        /**
         *  CHECK PROGRESS OF IMPORTING RECORDS  
         *  @return status of importing records
         */
        function checkProgress()
        {
            $.ajax({
                url: $(document).find("input[name='progressURL']").val(),
                type: 'GET',
                dataType: 'json',
               // showLoader: true,
                data: "action=check_progress",
                complete: function(response) 
                { debugger           
                    var response = response.responseJSON.output;
                    if(response.result)
                    {   
                        var info = response.info;
                        var entity_type = info.entity_type;
                        var barWidth = Math.round((parseInt(info.imported_records)/parseInt(info.total_records)) * 100);
                        $('#process_'+info.entity_type).find('.progress_width').animate({width: barWidth+'%'}, 500 );
                        $('#process_'+info.entity_type).find('.progress_width').html(barWidth+'%');
                        setTimeout(checkProgress,500);            
                    }             
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        }

    });
});