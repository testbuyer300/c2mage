<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Dotsquares\Cartmigrate\Block\Adminhtml\Index;

class Import extends \Magento\Backend\Block\Widget\Container
{

    /**
     * @var helperBridge
     */
    protected $helperBridge;

    public function __construct(
    	\Magento\Backend\Block\Widget\Context $context,
    	\Dotsquares\Cartmigrate\Helper\Bridge $helperBridge,
    	array $data = []
    )
    {
    	$this->helperBridge = $helperBridge;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function import()
    {
    	$cart_url = $this->getData('cart_url');
    	$cart_token = $this->getData('cart_token');
    	$action = $this->getData('action');

    	$response = $this->helperBridge->getSource($cart_url,$cart_token,$action);

    	return $response;      
    } 



}