<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Dotsquares\Cartmigrate\Block\Adminhtml\Index;

class Config extends \Magento\Backend\Block\Widget\Container
{
	/**
     * @var categoryCollection
     */
    protected $categoryCollection;

    /**
     * @var attributeSetsCollection
     */
    protected $attributeSetsCollection;

    /**
     * @var customerGroupColl
     */
    protected $customerGroupColl;

    /**
     * @var statusCollection
     */
    protected $statusCollection;

    /**
     * @var helperBridge
     */
    protected $helperBridge;

    public function __construct(
    	\Magento\Backend\Block\Widget\Context $context,
    	\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
    	\Magento\Catalog\Model\Product\AttributeSet\Options $attributeSetsCollection,
    	\Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupColl, 
    	\Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory,
    	\Dotsquares\Cartmigrate\Helper\Bridge $helperBridge,
    	array $data = []
    )
    {
    	$this->categoryCollection = $categoryCollection;
    	$this->attributeSetsCollection = $attributeSetsCollection;
    	$this->customerGroupColl = $customerGroupColl;
    	$this->statusCollection = $statusCollectionFactory;
    	$this->helperBridge = $helperBridge;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getRootCategories()
    {
    	$root_categories = $this->categoryCollection->create()
                          ->addAttributeToSelect('*')
                          ->addAttributeToFilter('level', 1)
                          ->addAttributeToFilter('is_active', 1);
    	return $root_categories;
        
    } 

    /**
     * @return array
     */
    public function getAttributeSets()
    {
    	$attribute_sets = $this->attributeSetsCollection->toOptionArray();
    	return $attribute_sets;      
    } 

    /**
     * @return array
     */
    public function getSourceUserRoles()
    {
    	$cart_url = $this->getData('cart_url');
    	$cart_token = $this->getData('cart_token');
    	$user_roles = $this->helperBridge->getSourceUserRoles($cart_url,$cart_token);
    	return $user_roles;      
    } 

    /**
     * @return array
     */
    public function getTargetCustomerGroups()
    {
    	$customer_groups = $this->customerGroupColl->toOptionArray();
    	return $customer_groups;      
    } 


    /**
     * @return array
     */
    public function getSourceOrderStatus()
    {
    	$sourceOrderStatus = array(
                       'wc-pending'    => 'Pending payment',
                       'wc-processing' => 'Processing',
                       'wc-on-hold'    => 'On hold',
                       'wc-completed'  => 'Completed',
                       'wc-cancelled'  => 'Cancelled',
                       'wc-refunded'   => 'Refunded',
                       'wc-failed'     => 'Failed'                
        );   
        return $sourceOrderStatus; 
    } 


    /**
     * @return array
     */
    public function getTargetOrderStatus()
    {
    	$order_statuses = $this->statusCollection->create()->toOptionArray();
    	return $order_statuses;      
    } 


    

}
