<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Dotsquares\Cartmigrate\Logger;


class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;
 
    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/migrate.log';
}

?>