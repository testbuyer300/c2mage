<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Dotsquares\Cartmigrate\Controller\Adminhtml\Index;

class Checkprogress extends \Magento\Backend\App\Action
{
    /**
     * @var resultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * @var connection
     */
    private $connection;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_session;

    /**
     * @var \Magento\Backend\Model\Session $backendSession,
     */
    protected $backendSession;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Model\Session $backendSession
    ) {

        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->connection = $resourceConnection;
        $this->backendSession = $backendSession;
    }


    public function execute()
    {
        if($this->getRequest()->isAjax())
        {
            $data = $this->getRequest()->getParams();
            $result = $this->resultJsonFactory->create();
            $resultPage = $this->resultPageFactory->create();

            $connection  = $this->connection->getConnection();
            $processLogTableName   = $connection->getTableName('cartmigrate_process_log');

            $mKey = $this->backendSession->getMigrationKey();
            $sql = "SELECT * FROM ".$processLogTableName." WHERE migration_key='".$mKey."' ORDER BY id DESC LIMIT 1";
            $response = $connection->fetchAll($sql);  

            if(count($response) > 0)
            {
                $response = array('result'=>'processing','info'=> $response[0]);

            }

            $result->setData(['output' => $response]);
            return $result;          
        }

        return false;
    }
}

?>