<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Dotsquares\Cartmigrate\Controller\Adminhtml\Index;

class Config extends \Magento\Backend\App\Action
{
    /**
     * @var resultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * @var helperBridge
     */
    protected $helperBridge;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Dotsquares\Cartmigrate\Helper\Bridge $helperBridge
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->helperBridge = $helperBridge;
    }

    public function execute()
    {
        if($this->getRequest()->isAjax())
        {
            $data = $this->getRequest()->getParams();
            $result = $this->resultJsonFactory->create();
            $resultPage = $this->resultPageFactory->create();

            // check security token
            $checkToken = $this->helperBridge->checkBridge($data['cart_url'],$data['cart_token']);

            // if source is correct and token is matched
            if($checkToken['result'] == 'success' )
            {
                $response = $resultPage->getLayout()
                        ->createBlock('\Dotsquares\Cartmigrate\Block\Adminhtml\Index\Config')
                        ->setTemplate('Dotsquares_Cartmigrate::config.phtml')
                        ->setData('cart_url',$data['cart_url'])
                        ->setData('cart_token',$data['cart_token'])
                        ->toHtml();
            }
            else
            {
                $response = array("result"=>'error',"message"=>$checkToken['message']);
            }

            $result->setData(['output' => $response]);
            return $result;          
        }

        return false;
    }
}

?>