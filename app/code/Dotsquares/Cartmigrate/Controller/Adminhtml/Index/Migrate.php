<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Dotsquares\Cartmigrate\Controller\Adminhtml\Index;


class Migrate extends \Magento\Backend\App\Action
{
    /**
     * @var resultJsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\Session $backendSession
     */
    protected $backendSession;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\Session $backendSession
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->backendSession = $backendSession;
    }

    public function execute()
    {
        if($this->getRequest()->isAjax())
        {
            $data = $this->getRequest()->getParams();
            $result = $this->resultJsonFactory->create();
            $resultPage = $this->resultPageFactory->create();

            if(isset($data['taxes']) &&  $data['taxes'] == 'on' )
            {
                $this->backendSession->setMigrationTaxes(1);
            }
            else
            {
                $this->backendSession->setMigrationTaxes(0);   
            }
            if(isset($data['manufacturers']) &&  $data['manufacturers'] == 'on' )
            {
                $this->backendSession->setMigrationManuf(1);
            } 
            else
            {
                $this->backendSession->setMigrationManuf(0);
            }
            if(isset($data['categories']) &&  $data['categories'] == 'on' )
            {
                $this->backendSession->setMigrationCat(1);
            } 
            else
            {
                $this->backendSession->setMigrationCat(0);
            }
            if(isset($data['products']) &&  $data['products'] == 'on' )
            {
                $this->backendSession->setMigrationProducts(1);
            } 
            else
            {
                $this->backendSession->setMigrationProducts(0);
            }
            if(isset($data['reviews']) &&  $data['reviews'] == 'on' )
            {
                $this->backendSession->setMigrationReviews(1);
            }
            else
            {
                $this->backendSession->setMigrationReviews(0);
            }
            if(isset($data['customers']) &&  $data['customers'] == 'on' )
            {
                $this->backendSession->setMigrationCustomers(1);
            } 
            else
            {
                $this->backendSession->setMigrationCustomers(0);
            }
            if(isset($data['orders']) &&  $data['orders'] == 'on' )
            {
                $this->backendSession->setMigrationOrders(1);    
            } 
            else
            {
                $this->backendSession->setMigrationOrders(0);
            }

            $this->backendSession->setDefaultCategory($data['category']);

            $block = $resultPage->getLayout()
                ->createBlock('Dotsquares\Cartmigrate\Block\Adminhtml\Index\Migrate')
                ->setTemplate('Dotsquares_Cartmigrate::migrate.phtml')
                ->setData('data',$data)
                ->toHtml();

            $result->setData(['output' => $block]);
            return $result;             
        }

        return false;
    }
}

?>