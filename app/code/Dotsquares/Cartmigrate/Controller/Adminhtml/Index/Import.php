<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Dotsquares\Cartmigrate\Controller\Adminhtml\Index;

class Import extends \Magento\Backend\App\Action
{

    /**
     * @var resultJsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * @var connection
     */
    private $connection;

    /**
     * @var \Magento\Framework\Session\SaveHandler
     */
    protected $sessionHandler;

    

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Session\SaveHandler $sessionHandler
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->connection = $resourceConnection;
        $this->sessionHandler = $sessionHandler;
    }

    public function execute()
    {
        // path of directory which saves session on server
        $tmp_session_dir = ini_get("session.save_path");
        $this->sessionHandler->close();
        $this->sessionHandler->open($tmp_session_dir,"admin");

    	if($this->getRequest()->isAjax())
        {
            $data = $this->getRequest()->getParams();
            $result = $this->resultJsonFactory->create();
            $resultPage = $this->resultPageFactory->create();

            $response = $resultPage->getLayout()
                    ->createBlock('\Dotsquares\Cartmigrate\Block\Adminhtml\Index\Import')
                    ->setData('cart_url',$data['cart_url'])
                    ->setData('cart_token',$data['cart_token'])
                    ->setData('action',$data['action'])
                    ->import();    

            $result->setData(['output' => $response]);
            return $result;             
        }

        return false;
    }
}