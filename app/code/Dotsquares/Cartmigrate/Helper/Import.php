<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Dotsquares\Cartmigrate\Helper;

class Import
{
    /**
     * @var $sourceTablePrefix
     */
	protected $targetTablePrefix;

    /**
     * @var connection
     */
    private $connection;

    /**
     * @var \Dotsquares\Cartmigrate\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Backend\Model\Session $backendSession
     */
    protected $backendSession;


    /**
     * @var  \Magento\Framework\Filesystem\DirectoryList
     */
    protected $dir;


    CONST CATEGORY_ENTITY_TYPE_ID = 3;

    

	public function __construct(
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Dotsquares\Cartmigrate\Logger\Logger $logger,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Framework\Filesystem\DirectoryList $dir
	)
	{
		$this->connection = $resourceConnection;
		$this->logger = $logger;
        $this->backendSession = $backendSession;
        $this->dir = $dir;
	}

	public function processImport($action,$info)
	{
        if($action == 'import_store_info')
        {
        	$response = $this->importStoreInfo($info);
        }
        if($action == 'import_tax')
        {
            $response = $this->importTax($info);
        }
        if($action == 'import_categories')
        {
            $response = $this->importCategories($info);
        }

        return $response;
	}
	

    /**
     * @return array
     */
    public function importStoreInfo($store_info)
    {    
    	$error = array();
    	$response = array();

        // set migration key in backend session
        $mKey = date('Y-m-d-H-i-s');
        $this->backendSession->setMigrationKey($mKey);

        // creating DB connection
    	$connection  = $this->connection->getConnection();
    	$tableName   = $connection->getTableName('core_config_data');


        // import store general data
    	foreach($store_info['store_info'] as $info)
    	{
            $path = '';

            // general store infomation
    		if($info['option_name'] == 'woocommerce_store_address') // store address line1
    		{
    			$path = 'general/store_information/street_line1';
    		}
    		if($info['option_name'] == 'woocommerce_store_address_2')  // store address line2
    		{
    			$path = 'general/store_information/street_line2';
    		}
    		if($info['option_name'] == 'woocommerce_store_city')  // store address city
    		{
    			$path = 'general/store_information/city';
    		}
    		if($info['option_name'] == 'woocommerce_default_country')  // store address country
    		{
    			$path = 'general/store_information/country_id';
    			$info['option_value'] = explode(':',$info['option_value']);
    			$info['option_value'] = $info['option_value'][0];
    		}
    		if($info['option_name'] == 'woocommerce_store_postcode')  // store address postcode
    		{
    			$path = 'general/store_information/postcode';
    		}

   			try
   			{
	    		$sql = "SELECT * FROM " . $tableName. " WHERE path = '".$path."' AND scope_id = 0";
	    		$result = $connection->fetchAll($sql);

	    		if(count($result) == 0)
	    		{
	    			$sql = "INSERT INTO " . $tableName. " SET value='".$info['option_value']."' , path='".$path."'";
	    		} 
	    		else
	    		{
	    			$sql = "UPDATE " . $tableName. " SET value='".$info['option_value']."' WHERE path='".$path."'";
	    		}
	    		$connection->query($sql);

	    		$this->logger->info("store general data imported");	
	    	}
	    	catch(\Exception $e)
	    	{
	    		$this->logger->info("Error in importing store general data- ".$e->getMessage());
	    		$response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
	    		return $response;
	    	}
    	}


        // import store/sales/contact email
        $email_info = $store_info['email_info'];
        foreach($email_info as $email)
        {
            try
            {
                $all_path = array();
                if($email['option_name'] == "woocommerce_email_from_name")
                {
                    $all_path = array("trans_email/ident_general/name","trans_email/ident_sales/name","trans_email/ident_support/name","");
                }
                if($email['option_name'] == "woocommerce_email_from_address")
                {
                    $all_path = array("trans_email/ident_general/email","trans_email/ident_sales/email","trans_email/ident_support/email","contact/email/recipient_email");
                }
                if($email['option_name'] == "woocommerce_new_order_settings")
                {
                    $all_path = array("sales_email/order/copy_to","sales_email/order_comment/copy_to","sales_email/invoice/copy_to","sales_email/invoice_comment/copy_to","sales_email/shipment/copy_to","sales_email/shipment_comment/copy_to","sales_email/creditmemo/copy_to","sales_email/creditmemo_comment/copy_to");

                    $email['option_value'] = unserialize($email['option_value']);
                    $email['option_value'] = $email['option_value']['recipient'];
                }

                foreach($all_path  as $path)
                {
                    $sql = "SELECT * FROM " . $tableName. " WHERE path = '".$path."' AND scope_id = 0";
                    $result = $connection->fetchAll($sql);

                    if(count($result) == 0)
                    {
                        $sql = "INSERT INTO " . $tableName. " SET value='".$email['option_value']."' , path='".$path."'";
                    } 
                    else
                    {
                        $sql = "UPDATE " . $tableName. " SET value='".$email['option_value']."' WHERE path='".$path."'";
                    }
                    $connection->query($sql);
                } 


                $this->logger->info("store store/sales email data imported"); 
            }
            catch(\Exception $e)
            {
                $this->logger->info("Error in importing store/sales email data- ".$e->getMessage());  
                $response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
                return $response;
            }
        }
 

        // import currency info
        $currency_code = $store_info['currency_info'][0]['option_value'];
        $currency_paths = array("currency/options/base","currency/options/default","currency/options/allow");

        foreach($currency_paths as $path)
        {
            try
            {
                $sql = "SELECT * FROM " . $tableName. " WHERE path = '".$path."' AND scope_id = 0";
                $result = $connection->fetchAll($sql);

                if(count($result) == 0)
                {
                    $sql = "INSERT INTO " . $tableName. " SET value='".$currency_code."' , path='".$path."'";
                } 
                else
                {
                    $sql = "UPDATE " . $tableName. " SET value='".$currency_code."' WHERE path='".$path."'";
                }
                $connection->query($sql);

                $this->logger->info("store currency data imported"); 
            }
            catch(\Exception $e)
            {
                $this->logger->info("Error in importing store currency- ".$e->getMessage());  
                $response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
                return $response;
            }
        }


    	if(empty($error))
    	{
    		$response = array('status_code'=>200, 'error'=>0, 'status'=>'success', 'message'=>'store info successfully imported.');
    	}

        return $response;
    }


    /**
     * @return array
     */
    public function importTax($tax_info)
    {  
        $error = array();
        $response = array();

        // creating DB connection
        $connection  = $this->connection->getConnection();
        $tableName   = $connection->getTableName('core_config_data');

        foreach($tax_info['tax_info'] as $taxinfo)
        {
            $all_path = array();
            if($taxinfo['option_name'] == 'woocommerce_prices_include_tax')
            {
                $all_path = array(
                           "tax/calculation/price_includes_tax" =>array("yes"=>1,"no"=>0),
                           "tax/calculation/shipping_includes_tax"=>array("yes"=>1,"no"=>0)
                       );
            }
            if($taxinfo['option_name'] == 'woocommerce_tax_based_on')
            {
                $all_path = array("tax/calculation/based_on"=>array("shipping"=>"shipping","billing"=>"billing","base"=>"origin")
                        );
            }
            if($taxinfo['option_name'] == 'woocommerce_tax_display_shop')
            {
                $all_path = array(
                            "tax/display/type"     =>array("excl"=>1,"incl"=>2),
                            "tax/display/shipping" =>array("excl"=>1,"incl"=>2)
                        );
            }
            if($taxinfo['option_name'] == 'woocommerce_tax_display_cart')
            {
                $all_path = array(
                            "tax/cart_display/price"     =>array("excl"=>1,"incl"=>2),
                            "tax/cart_display/subtotal"  =>array("excl"=>1,"incl"=>2),
                            "tax/cart_display/shipping"  =>array("excl"=>1,"incl"=>2),
                            "tax/sales_display/price"    =>array("excl"=>1,"incl"=>2),
                            "tax/sales_display/subtotal" =>array("excl"=>1,"incl"=>2),
                            "tax/sales_display/shipping" =>array("excl"=>1,"incl"=>2),
                        );
            }

            
            foreach($all_path as $path => $value)
            {
    
                try
                {
                    $sql = "SELECT * FROM " . $tableName. " WHERE path = '".$path."' AND scope_id = 0";
                    $result = $connection->fetchAll($sql);

                    $mapped_value = $taxinfo['option_value'];            
                
                    if(count($result) == 0)
                    {
                        $sql = "INSERT INTO " . $tableName. " SET value='".$value[$mapped_value]."' , path='".$path."'";
                    } 
                    else
                    {
                        $sql = "UPDATE " . $tableName. " SET value='".$value[$mapped_value]."' WHERE path='".$path."'";
                    }
                    $connection->query($sql);

                    $this->logger->info("tax general data imported"); 
                }
                catch(\Exception $e)
                {
                    $this->logger->info("Error in importing tax general data- ".$e->getMessage());  
                    $response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
                    return $response;
                }
            }
        } // end foreach



        $tax_classTable =  $connection->getTableName('tax_class');
        $tax_ruleTable  =  $connection->getTableName('tax_calculation_rule');
        $tax_rateTable  =  $connection->getTableName('tax_calculation_rate');
        $tax_calcTable  =  $connection->getTableName('tax_calculation');

        $mKey = $this->backendSession->getMigrationKey();
        $processLogTable = $connection->getTableName('cartmigrate_process_log');
        $sql = "SELECT * FROM ". $processLogTable. " WHERE entity_type='taxes' AND migration_key='".$mKey."'";
        $record = $connection->fetchAll($sql);

        if(count($record) > 1 && ! empty($record))
        {
            $sql = "DELETE FROM ". $processLogTable. " WHERE entity_type='taxes' AND migration_key='".$mKey."'";
            $connection->query($sql);
        }
        // insert into log table
        $sql = "INSERT INTO " . $processLogTable. " SET entity_type='taxes', 
                                                        migration_key='".$mKey."', 
                                                        total_records='".count($tax_info['tax_rates'])."',
                                                        start_time='".microtime(true)."',
                                                        status='processing'";
        $connection->query($sql);
   
        $imported_records = 1;
        foreach($tax_info['tax_rates'] as $taxrate)
        {
            try
            {
                $tax_classTitle = $taxrate['tax_rate_class'];
                if($taxrate['tax_rate_class'] == '')
                {
                    $tax_classTitle = 'Standard Rates';
                }

                // import into table "tax_class" 
                $sql1 = "INSERT INTO " . $tax_classTable. " SET class_name='".$tax_classTitle."' , class_type='PRODUCT'";
                $connection->query($sql1);
                $last_insert_classId = $connection->lastInsertId();

                // import into table "tax_calculation_rule" 
                $tax_code = $tax_classTitle."-Rate ".$tax_classTitle;
                $sql2 = "INSERT INTO " . $tax_ruleTable . " SET code='".$tax_code."'";
                $connection->query($sql2);
                $last_insert_ruleId = $connection->lastInsertId();

                // import into table "tax_calculation_rate" 
                $sql3 = "INSERT INTO " . $tax_rateTable . " SET tax_country_id='".$taxrate['tax_rate_country']."', 
                                                                tax_region_id='".$taxrate['tax_rate_state']."', 
                                                                tax_postcode='".$taxrate['location_code']."',
                                                                code='".$tax_classTitle."', 
                                                                rate='".$taxrate['tax_rate']."'";     
                $connection->query($sql3);
                $last_insert_rateId = $connection->lastInsertId();


                // select customer tax class frm table "tax_class"
                $sql4 = "SELECT class_id FROM ".$tax_classTable." WHERE class_type='CUSTOMER' LIMIT 1" ;
                $customer_classId = $connection->fetchAll($sql4);
                $customer_classId = $customer_classId[0]['class_id'];


                // import into table "tax_calculation" 
                $sql5 = "INSERT INTO " . $tax_calcTable. " SET tax_calculation_rate_id='".$last_insert_rateId."', 
                                                               tax_calculation_rule_id='".$last_insert_ruleId."',
                                                               customer_tax_class_id='".$customer_classId."',
                                                               product_tax_class_id='".$last_insert_classId."'";
                $connection->query($sql5);


                // update log table
                $sql = "UPDATE " . $processLogTable. " SET imported_records='".$imported_records."', 
                                                           last_insert_id='".$last_insert_classId."' 
                                                           WHERE entity_type='taxes' AND migration_key='".$mKey."'";            
                $connection->query($sql);
                $imported_records++;

                $this->logger->info("tax class/rule data imported"); 
            }
            catch(\Exception $e)
            {
                $this->logger->info("Error in importing tax rule/class data- ".$e->getMessage());  
                $response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
                return $response;
            }

           //sleep(10);

        } // end foreach


        // update log table
        $sql = "UPDATE " . $processLogTable. " SET status='completed' WHERE entity_type='taxes' AND migration_key='".$mKey."'";         
        $connection->query($sql);


        $response = array('status_code'=>200, 'error'=>0, 'status'=>'success', 'message'=>'tax info successfully imported.');
        return $response;
    }


    /**
     * @return array
     */
    public function importCategories($cat_info)
    { 
       
        $response = array();

        // creating DB connection
        $connection  = $this->connection->getConnection();

        // get all category tables
        $eav_attrTable =  $connection->getTableName('eav_attribute');
        $category_entityTable =  $connection->getTableName('catalog_category_entity');
        $category_datetimeTable  =  $connection->getTableName('catalog_category_entity_datetime');
        $category_decimalTable  =  $connection->getTableName('catalog_category_entity_decimal');
        $category_intTable  =  $connection->getTableName('catalog_category_entity_int');
        $category_textTable  =  $connection->getTableName('catalog_category_entity_text');
        $category_varcharTable  =  $connection->getTableName('catalog_category_entity_varchar');

        $cat_map_array = array();

        // get current migration key
        $mKey = $this->backendSession->getMigrationKey();
        $processLogTable = $connection->getTableName('cartmigrate_process_log');
        $sql = "SELECT * FROM ". $processLogTable. " WHERE entity_type='categories' AND migration_key='".$mKey."'";
        $record = $connection->fetchAll($sql);

        if(count($record) > 1 && ! empty($record))
        {
            $sql = "DELETE FROM ". $processLogTable. " WHERE entity_type='categories' AND migration_key='".$mKey."'";
            $connection->query($sql);
        }


        // insert into log table
        $sql = "INSERT INTO " . $processLogTable. " SET entity_type='categories', 
                                                        migration_key='".$mKey."', 
                                                        total_records='".count($cat_info['cat_info'])."',
                                                        start_time='".microtime(true)."',
                                                        status='processing'";
        $connection->query($sql);
   
        $imported_records = 1;

        foreach($cat_info['cat_info'] as $catInfo)
        {
            try
            {

                /* INSERT DATA INTO TABLE "catalog_category_entity" */
                $sql = "INSERT INTO " . $category_entityTable. " SET attribute_set_id='".self::CATEGORY_ENTITY_TYPE_ID."'";
                $connection->query($sql);
                $last_insert_catId = $connection->lastInsertId();
                $cat_map_array[$catInfo['term_id']] = $last_insert_catId;

                // default root category id
                $defaultCategoryId = $this->backendSession->getDefaultCategory();  // default category as parent 

                $treePath_array = array($last_insert_catId); //35
                $parent_id = $defaultCategoryId;
                if($catInfo['parent'] && $catInfo['parent']!=0)
                {
                    $parent_id = $cat_map_array[$catInfo['parent']];
                    array_push($treePath_array,$parent_id);  // 1/2/36
                    $treePath_array = $this->treePath($treePath_array,$parent_id,$defaultCategoryId);
                }
                array_push($treePath_array,$defaultCategoryId,1);


                $level = count($treePath_array) -1 ;
                $treePath_array = implode('/',array_reverse($treePath_array)); 

                $sql = "UPDATE " . $category_entityTable. " SET parent_id='".$parent_id."', 
                                                                path='".$treePath_array."' ,
                                                                level='".$level."' 
                                                                WHERE entity_id='".$last_insert_catId."'";
                $connection->query($sql);
           
        
                // get all category attribute ids 
                $sql = "SELECT attribute_id,attribute_code FROM ".$eav_attrTable." WHERE entity_type_id='".self::CATEGORY_ENTITY_TYPE_ID."' AND ( attribute_code='name' OR 
                            attribute_code='meta_title' OR 
                            attribute_code='url_path' OR 
                            attribute_code='url_key' OR
                            attribute_code='description' OR
                            attribute_code='meta_description' OR
                            attribute_code='is_active' OR 
                            attribute_code='is_anchor' OR
                            attribute_code='include_in_menu' OR
                            attribute_code='image')" ;
                $all_records = $connection->fetchAll($sql);

                $name_attrId = '';
                $meta_title_attrId = '';
                $url_path_attrId = '';
                $url_key_attrId = '';
                $description_attrId = '';
                $meta_desc_attrId = '';
                $is_active_attrId = '';
                $is_anchor_attrId = '';
                $include_menu_attrId = '';
                $image_attId='';
                foreach($all_records as $record)
                {
                    if($record['attribute_code'] == 'name'){ $name_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'meta_title'){ $meta_title_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'url_path'){ $url_path_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'url_key'){ $url_key_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'description'){ $description_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'meta_description'){ $meta_desc_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'is_active'){ $is_active_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'is_anchor'){ $is_anchor_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'include_in_menu'){ $include_menu_attrId = $record['attribute_id']; }
                    if($record['attribute_code'] == 'image'){ $image_attId = $record['attribute_id']; }
                }

                /**
                 * INSERT DATA INTO TABLE "catalog_category_entity_varchar" 
                 */
                // name
                $sql = "INSERT INTO " . $category_varcharTable. " SET attribute_id ='".$name_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = '".$catInfo['name']."' ";
                $connection->query($sql);
                // meta_title
                $sql = "INSERT INTO " . $category_varcharTable. " SET attribute_id ='".$meta_title_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = '".$catInfo['name']."' ";
                $connection->query($sql);
                // url_key
                $sql = "INSERT INTO " . $category_varcharTable. " SET attribute_id ='".$url_path_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = '".$catInfo['slug']."' ";
                $connection->query($sql);

                // image
                if($catInfo['image'] != '')
                {
                    $catimgName  = basename($catInfo['image']);
                    $sql = "INSERT INTO " . $category_varcharTable. " SET attribute_id ='".$image_attId."',
                                                                          store_id = 0,
                                                                          entity_id = '".$last_insert_catId."',
                                                                          value = '".$catimgName."' ";
                    $connection->query($sql);

                    // get image from source
                    $catImg = file_get_contents($catInfo['image']); 
                    file_put_contents($this->dir->getPath('media').'/catalog/category/'.$catimgName, $catImg);
                    @chmod($this->dir->getPath('media').'/catalog/category/'.$catimgName,07777);
                }                                                      
            

                /**
                 * INSERT DATA INTO TABLE "catalog_category_entity_text" 
                 */
                // description
                $sql = "INSERT INTO " . $category_textTable. " SET attribute_id ='".$description_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = '".$catInfo['description']."' ";
                $connection->query($sql);
                // meta_description
                $sql = "INSERT INTO " . $category_textTable. " SET attribute_id ='".$meta_desc_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = '".$catInfo['description']."' ";
                $connection->query($sql);


                /** 
                 *  INSERT DATA INTO TABLE "catalog_category_entity_int" 
                 */
                // is_active
                $sql = "INSERT INTO " . $category_intTable. " SET attribute_id ='".$is_active_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = 1 ";
                $connection->query($sql);
                // is_anchor
                $sql = "INSERT INTO " . $category_intTable. " SET attribute_id ='".$is_anchor_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = 1 ";
                $connection->query($sql);
                // include_in_menu
                $sql = "INSERT INTO " . $category_intTable. " SET attribute_id ='".$include_menu_attrId."',
                                                                      store_id = 0,
                                                                      entity_id = '".$last_insert_catId."',
                                                                      value = 1 ";
                $connection->query($sql);


                // update log table
                $sql = "UPDATE " . $processLogTable. " SET imported_records='".$imported_records."', 
                                                           last_insert_id='".$last_insert_catId."' 
                                                           WHERE entity_type='categories' AND migration_key='".$mKey."'";            
                $connection->query($sql);
                $imported_records++;

                $this->logger->info("categories data imported. Category name = ".$catInfo['name']." id = ".$last_insert_catId ); 
            }
            catch(\Exception $e)
            {
                $this->logger->info("Error in importing categories data- ".$e->getMessage());  
                $response = array('status_code'=>404, 'error'=>1, 'status'=>'failed', 'message'=>$e->getMessage()); 
                return $response;
            }

        }// end foreach


        // update log table
        $sql = "UPDATE " . $processLogTable. " SET status='completed' WHERE entity_type='categories' AND migration_key='".$mKey."'";         
        $connection->query($sql);

        $response = array('status_code'=>200, 'error'=>0, 'status'=>'success', 'message'=>'categories successfully imported.');
        return $response;
    }


    public function treePath($treePath_array,$parent_id,$defaultCategoryId)
    {
        // creating DB connection
        $connection  = $this->connection->getConnection();
        $category_entityTable =  $connection->getTableName('catalog_category_entity');

        $sql = "SELECT parent_id FROM ".$category_entityTable." WHERE entity_id='".$parent_id."'" ;
        $record = $connection->fetchAll($sql);

        if($record[0]['parent_id']!=0 && $record[0]['parent_id']!=$defaultCategoryId && $record[0]['parent_id']!=1 )
        {
            array_push($treePath_array,$record[0]['parent_id'] );
            $this->treePath($treePath_array,$record[0]['parent_id']);
        }

        return $treePath_array;
    }

}