<?php
/**
 * @author     DOTSQUARES
 * @package    Dotsquares_Cartmigrate
 * @copyright  Copyright (c) 2018 Dotsquares Ltd. (https://www.dotsquares.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Dotsquares\Cartmigrate\Helper;

class Bridge
{
	
	const BRIDGE_FILE_PATH = '/migration/wp_bridge.php'; 

	/**
     * @var $sourceTablePrefix
     */
	protected $sourceTablePrefix;

	/**
     * @var \Dotsquares\Cartmigrate\Helper\Import
     */
    protected $helperImport;

    /**
     * @var \Magento\Backend\Model\Session $backendSession,
     */
    protected $backendSession;



	public function __construct(
		\Dotsquares\Cartmigrate\Helper\Import $helperImport,
		\Magento\Backend\Model\Session $backendSession
	)
	{
		$this->helperImport = $helperImport;
		$this->backendSession = $backendSession;
	}


    /**
     * @return array
     */
	public function checkBridge($cart_url,$cart_token)
	{
		// check if source file exist on root folder
		$url = $cart_url.self::BRIDGE_FILE_PATH;
		$curl = curl_init($url);
	    curl_setopt($curl, CURLOPT_NOBODY, true);
	    curl_exec($curl);
	    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    curl_close($curl);

	    if( $httpCode != 200 )
	    {
	    	return array("result"=>"error","message"=>"Kindly check connection file exist on root of source");
	    }

        // check token validity
		$curl = curl_init();
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $cart_url.self::BRIDGE_FILE_PATH.'?token='.$cart_token.'&action=check',
	        CURLOPT_POST => 1,
	        CURLOPT_POSTFIELDS => array()
	    ));
	    $response = curl_exec($curl);
	    $response = unserialize(base64_decode($response));

	    // getting source table prefix
        if(isset($response['result']) && $response['result']=='success')
        {
        	$this->sourceTablePrefix = $response['object']['table_prefix'];
        }

	    return $response ;
	}


    /**
     * @return array
     */
	public function openBridge($cart_url,$cart_token,$action,$queries)
	{
 	
	    $queries = base64_encode(serialize($queries));
        $curl = curl_init();
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $cart_url.self::BRIDGE_FILE_PATH.'?token='.$cart_token.'&action='.$action,
	        CURLOPT_POST => 1,
	        CURLOPT_POSTFIELDS => array(
						            'serialize' => true,
						            'query' => $queries
						        )
	    ));

	    $result = curl_exec($curl);

		$result = unserialize(base64_decode($result));

		if($result['result'] == 'success')
		{
			return $result['object']; 
		}
	}


	/**
     * @return array
     */
    public function getSourceUserRoles($cart_url,$cart_token)
    {
        $this->checkBridge($cart_url,$cart_token);
      
    	$table_prefix = $this->sourceTablePrefix;
    	$action = 'query';
    	$queries  = array(                 
	                    "user_roles" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE option_name ='wp_user_roles'"
	                    )
                	);
    	$user_roles = $this->openBridge($cart_url,$cart_token,$action,$queries);
    	return unserialize($user_roles["user_roles"][0]['option_value']);
    } 


    /**
     * @return array
     */
    public function getSource($cart_url,$cart_token,$action)
    {
        $response = null;

        if($action == 'clear_store' )
        {
        	$response = $this->clearTargetStore($cart_url,$cart_token);
        } 
        if($action == 'import_store' )
        {
        	$response = $this->getSourceStoreInfo($cart_url,$cart_token);
        } 
        elseif($action == 'import_tax'        && $this->backendSession->getMigrationTaxes()==1)
        {
        	$response = $this->getSourceTaxInfo($cart_url,$cart_token);
        }
        elseif($action == 'import_categories' && $this->backendSession->getMigrationCat()==1)
        {
        	$response = $this->getSourceCategoryInfo($cart_url,$cart_token);
        }
        else
        {
        	$response = array("result"=>"no import");
        }
        return  $response;
    } 


    /**
     * @return array
     */
    public function clearTargetStore($cart_url,$cart_token)
    {  
    	$this->checkBridge($cart_url,$cart_token);
    	$table_prefix = $this->sourceTablePrefix;

    	if($this->backendSession->getMigrationTaxes()==1)
    	{
    		$action = 'query';
    		$queries  = array(                 
	                    "clear_tax" => array(
	                        "type"=>"query",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                                  `option_name`= 'woocommerce_store_address' || 
	                                  `option_name`= 'woocommerce_store_address_2' || 
	                                  `option_name`= 'woocommerce_store_city' || 
	                                  `option_name`= 'woocommerce_default_country' ||
	                                  `option_name`= 'woocommerce_store_postcode'" 
	                    )
                	);
    		//$store_info = $this->openBridge($cart_url,$cart_token,$action,$queries);
    	}


    }


    /**
     * @return array
     */
    public function getSourceStoreInfo($cart_url,$cart_token)
    {    
        $this->checkBridge($cart_url,$cart_token);
    	$table_prefix = $this->sourceTablePrefix;

    	$action = 'query';
    	$queries  = array(                 
	                    "store_info" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                                  `option_name`= 'woocommerce_store_address' || 
	                                  `option_name`= 'woocommerce_store_address_2' || 
	                                  `option_name`= 'woocommerce_store_city' || 
	                                  `option_name`= 'woocommerce_default_country' ||
	                                  `option_name`= 'woocommerce_store_postcode'" 
	                    ),
	                    "currency_info" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                                  `option_name`= 'woocommerce_currency'"
	                    ),
	                    "email_info" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                                  `option_name`= 'woocommerce_email_from_name'||
	                                  `option_name`= 'woocommerce_email_from_address' ||
	                                  `option_name`= 'woocommerce_new_order_settings'"
	                    )
                	);
    	$store_info = $this->openBridge($cart_url,$cart_token,$action,$queries);

    	$response = $this->helperImport->processImport('import_store_info',$store_info);
    	return $response;
    } 


    /**
     * @return array
     */
    public function getSourceTaxInfo($cart_url,$cart_token)
    {    
    	$this->checkBridge($cart_url,$cart_token);
    	$table_prefix = $this->sourceTablePrefix;

    	$action = 'query';
    	$queries  = array(                 
	                    "tax_info" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                                  `option_name`= 'woocommerce_prices_include_tax' || 
	                                  `option_name`= 'woocommerce_tax_based_on' || 
	                                  `option_name`= 'woocommerce_tax_display_shop' || 
	                                  `option_name`= 'woocommerce_tax_display_cart'" 
	                    ),
	                    "tax_rates" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT tr.*,trl.location_code FROM ".$table_prefix."woocommerce_tax_rates  as tr LEFT JOIN ".$table_prefix."woocommerce_tax_rate_locations as trl ON tr.tax_rate_id = trl.tax_rate_id" 
	                    )
	                    // "email_info" => array(
	                    //     "type"=>"select",
	                    //     "query"=> "SELECT * FROM ".$table_prefix."options WHERE 
	                    //               `option_name`= 'woocommerce_email_from_name'||
	                    //               `option_name`= 'woocommerce_email_from_address' ||
	                    //               `option_name`= 'woocommerce_new_order_settings'"
	                    // )
                	);
    	$tax_info = $this->openBridge($cart_url,$cart_token,$action,$queries);

    	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/tax.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($tax_info);

    	$response = $this->helperImport->processImport('import_tax',$tax_info);
    	return $response;
    }


    /**
     * @return array
     */
    public function getSourceCategoryInfo($cart_url,$cart_token)
    {    
    	$this->checkBridge($cart_url,$cart_token);
    	$table_prefix = $this->sourceTablePrefix;

    	$action = 'query';
    	$queries  = array(                 
	                    "cat_info" => array(
	                        "type"=>"select",
	                        "query"=> "SELECT termTax.term_id,
	                                          termTax.term_taxonomy_id,
	                                          termTax.parent,
	                                          termTax.description,
	                                          terms.name,
	                                          terms.slug 
	                                          FROM ".$table_prefix."term_taxonomy as termTax 
	                                          LEFT JOIN ".$table_prefix."terms as terms
	                                          ON termTax.term_id = terms.term_id 
	                                          WHERE termTax.taxonomy ='product_cat'"
	                    )
                	);
    	$cat_info = $this->openBridge($cart_url,$cart_token,$action,$queries);

    	// remove "uncategorized" category
     	array_shift($cat_info['cat_info']);

    	
        // getting category image
        if(count($cat_info['cat_info']) > 0)
        {
        	$count = 0;
	    	foreach($cat_info['cat_info'] as $cat)
	    	{
	    		    
	    			$cat_info['cat_info'][$count]['display_type'] = '';
	    			$cat_info['cat_info'][$count]['image'] = '';

	    			$queries  = array(                 
		                    "cat_meta" => array(
		                        "type"=> "select",
		                        "query"=> "SELECT guid FROM ".$table_prefix."posts 
		                        		   WHERE ID=( SELECT meta_value FROM ".$table_prefix."termmeta 
		                        		   WHERE meta_key='thumbnail_id' AND term_id = ".$cat['term_id'].")"	                        
		                    )
	                    );
	    			$cat_meta = $this->openBridge($cart_url,$cart_token,$action,$queries);

                    if(count($cat_meta['cat_meta']) > 0)
                    {
                        foreach($cat_meta['cat_meta'] as $cat_meta)
                        {
                        	if(isset($cat_meta['guid']))
                        	{
                        		$cat_info['cat_info'][$count]['image'] = $cat_meta['guid'];
                        	}
                      		// if(isset($cat_meta['meta_key']) && $cat_meta['meta_key'] == "display_type")
                      		// {
                      		// 	$cat_info['cat_info'][$count]['display_type'] = $cat_meta['meta_value'];
                      		// }
                      		// if(isset($cat_meta['meta_key']) && $cat_meta['meta_key'] == "thumbnail_id")
                      		// {
                      		// 	$cat_info['cat_info'][$count]['thumbnail_id'] = $cat_meta['meta_value'];
                      		// }

                        }
                    }


	    			$count++;
	    	}
	    }

    	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cat.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($cat_info);

		$response = $this->helperImport->processImport('import_categories',$cat_info);
    	return $response;
    }
   



}