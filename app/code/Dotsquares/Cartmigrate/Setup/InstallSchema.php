<?php

namespace Dotsquares\Cartmigrate\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // CREATE "cartmigrate_process_log" TABLE
        $tableName = $installer->getTable('cartmigrate_process_log');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
           
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'entity_type',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => '',],
                    'Entity Type'
                )
                ->addColumn(
                    'migration_key',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => '',],
                    'Migration Key'
                )
                ->addColumn(
                    'total_records',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Total Records'
                )
                ->addColumn(
                    'imported_records',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Imported Records'
                )
                ->addColumn(
                    'start_time',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Start Time'
                )
                ->addColumn(
                    'last_insert_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Imported Records'
                )
                ->addColumn(
                    'errors',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false,'default' => 0],
                    'Imported Records'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Status'
                )
                ->addIndex(
                    $installer->getIdxName(
                        $tableName,
                        ['entity_type','migration_key'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['entity_type','migration_key'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->setComment('process log')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }



         // CREATE "cartmigrate_process_error_log" TABLE
        $tableName = $installer->getTable('cartmigrate_process_error_log');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
           
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'entity_type_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false,'unsigned' => true,],
                    'Entity Type Id'
                )
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Entity Id'
                )
                ->addColumn(
                    'error',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Error'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Status'
                )
                ->addForeignKey(
	                $installer->getFkName(
	                    'cartmigrate_process_error_log',
	                    'entity_type_id',
	                    'cartmigrate_process_log',
	                    'id'
	                ),
	                'entity_type_id',
	                $installer->getTable('cartmigrate_process_log'), 
	                'id',
	                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE, 
	                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
	            )
                ->setComment('process log')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }

    
}
