<?php

namespace Dots\Socialfeeds\Helper;

use Magento\Store\Model\ScopeInterface;
use Dots\Socialfeeds\Helper\Data as HelperData;

/**
 * Class Social
 *
 * @package Dots\Socialfeeds\Helper
 */
class Social extends HelperData
{
    
	const LINKEDIN_API_BASE        =   'https://api.linkedin.com/v1';
    const LINKEDIN_OAUTH_BASE      =   'https://www.linkedin.com/uas/oauth2';
    const LINKEDIN_CLIENT_ID       =   'social_feeds/linkedin/client_id';
    const LINKEDIN_CLIENT_SECRET   =   'social_feeds/linkedin/client_secret';

   /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAuthUrl($type)
    {
        $url = '';
        switch ($type) {
            case 'facebook':
                $url = '';
                break;
            case 'linkedin':
                $client_id = $this->getConfigValue(self::LINKEDIN_CLIENT_ID);
                $state     = uniqid('', true);
                $redirectUrl = $this->getRedirectUrl('linkedin');
                $url = self::LINKEDIN_OAUTH_BASE."/authorization?response_type=code&client_id={$client_id}&state={$state}&redirect_uri={$redirectUrl}";
                break;
            default:
                $url = '';
        }

        return $url;
        
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getRedirectUrl($type)
    {
        $url = $this->getBaseAuthUrl();
        switch ($type) {
            case 'facebook':
                $param = 'auth_done=facebook';
                break;
            case 'linkedin':
                $param = 'auth_done=linkedin';
                break;
            default:
                $param = '';
        }

        return $url. ($param ? (strpos($url, '?') ? '&' : '?') . $param : '');
        
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBaseAuthUrl()
    {
        $storeId = $this->getScopeUrl();
        /** @var \Magento\Store\Model\Store $store */
        $store = $this->storeManager->getStore($storeId);

        return $this->_getUrl('socialfeeds/social/callback', [
            '_nosid'  => true,
            '_scope'  => $storeId,
            '_secure' => $store->isUrlSecure()
        ]);
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getScopeUrl()
    {
        $scope = $this->_request->getParam(ScopeInterface::SCOPE_STORE) ?: $this->storeManager->getStore()->getId();

        if ($website = $this->_request->getParam(ScopeInterface::SCOPE_WEBSITE)) {
            $scope = $this->storeManager->getWebsite($website)->getDefaultStore()->getId();
        }

        return $scope;
    }
}
