<?php


namespace Dots\Socialfeeds\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 *
 * @package Dots\Socialfeeds\Helper
 */
class Data extends AbstractHelper
{
	/**
	 * @type \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @type \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;


	/**
	 * @param \Magento\Framework\App\Helper\Context $context
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 */
	public function __construct(
		Context $context,
		ScopeConfigInterface $scopeConfig,
		StoreManagerInterface $storeManager
	)
	{
		$this->scopeConfig  = $scopeConfig;
		$this->storeManager  = $storeManager;
		parent::__construct($context);
	}


 	/**
	 * @param $field
	 * @param null $storeId
	 * @return mixed
	 */
	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field,
			ScopeInterface::SCOPE_STORE,
			$storeId
		);
	}

}
