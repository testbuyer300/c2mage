<?php

namespace Dots\Socialfeeds\Block\System;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field as FormField;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Dots\Socialfeeds\Helper\Social as SocialHelper;

/**
 * Class AccessToken
 *
 * @package Dots\Socialfeeds\Block\System
 */
class AccessToken extends FormField
{
    /**
     * @type Dots\Socialfeeds\Helper\Social
     */
    protected $socialHelper;

    /**
     * AccessToken constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Dots\Socialfeeds\Helper\Social $socialHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        SocialHelper $socialHelper,
        array $data = []
    )
    {
        $this->socialHelper = $socialHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element)
    {
       $elementId   = explode('_', $element->getHtmlId());
       $authUrl = $this->socialHelper->getAuthUrl($elementId[2]);

       $html = '<a href="'.$authUrl.'" target="_new" class="action-default scalable">Get Access Token</a>';
       return $html;
    }

}
