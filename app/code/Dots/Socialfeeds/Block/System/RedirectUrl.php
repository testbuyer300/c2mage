<?php

namespace Dots\Socialfeeds\Block\System;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field as FormField;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Dots\Socialfeeds\Helper\Social as SocialHelper;

/**
 * Class Redirect
 *
 * @package Dots\Socialfeeds\Block\System
 */
class RedirectUrl extends FormField
{
    /**
     * @type Dots\Socialfeeds\Helper\Social
     */
    protected $socialHelper;

    /**
     * RedirectUrl constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Dots\Socialfeeds\Helper\Social $socialHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        SocialHelper $socialHelper,
        array $data = []
    )
    {
        $this->socialHelper = $socialHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $elementId   = explode('_', $element->getHtmlId());
        $redirectUrl = $this->socialHelper->getRedirectUrl($elementId[2]);
        $html = '<input type="text" value="'.$redirectUrl.'" readonly class="input-text admin__control-text">';

        return $html;
    }
}
