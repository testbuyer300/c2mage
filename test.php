<?php
$params =  array(
	'currency' => 'EUR',
	'item_name_1' => '5Test Product',
	'item_number_1' => '1',
	'item_quantity_1' => '1',
	'item_amount_1' => '50.00',
	'numberofitems' => '1',
	'encoding' => 'utf-8',
	'merchant_id' => '7411901079511710635',
	'merchant_site_id' => '155193',
	'time_stamp' => '2018-08-14.03:56:40',
	'version' => '4.0.0',
	'user_token_id' => 'testbuyer51@gmail.com',
	'user_token' => 'auto',
	'total_amount' => '50.00',
	//'notify_url' => 'https://sandbox.safecharge.com/lib/demo_process_request/response.php'
	);

array_map('trim', $params);

$JoinedInfo = join('', array_values($params));

$params["checksum"] = hash("sha256","oJdvtTQUmeuFyf9yokCQcVGoSSfSad2RTu23s1aBunm4KIiDSV4hEmVyBgcyGirv" . $JoinedInfo);

$fields_string="";
foreach($params as $key=>$value) { $fields_string .= $key."=".urlencode($value)."&"; }
$fields_string = rtrim($fields_string, "&");

header("Location:"."https://ppp-test.safecharge.com/ppp/purchase.do?".$fields_string);



?>


